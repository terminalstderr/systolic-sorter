// 2016 Ryan Leonard
// Basic Systolic Sorting Array (2 elements) Test Bench
`timescale 1ns / 1ns
module test_ssa2;

reg         clock;
reg         reset;
reg   [7:0] left_in;
wire  [7:0] left_out;

systolic_sort_array_2 dut(
  .clk	      (clock),
  .reset      (reset),
  .in         (left_in), 
  .out        (left_out),
  .finish     (finish)
);

initial // Drive the clock
begin
  #5 clock = 0;
  forever
    #5 clock = ~clock;
end

task test_sequence(
  input [7:0] a,
  input [7:0] b, 
  input [7:0] c,
  input [7:0] d);
begin
  $display("Reset Array");
  reset=1;  #10;

  $display("Inputting sequence: %d, %d, %d, %d", a, b, c, d);
  reset=0;
  left_in=a;    #10;
  left_in=b;    #10;
  left_in=c;    #10;
  left_in=d;    #10;

  $display("Flushing Array");
  left_in=255;  #40;
end
endtask

initial	// Test stimulus
begin
  reset=1;  #10;
  test_sequence(6, 5, 4, 3);
  test_sequence(20, 50, 4, 200);
  test_sequence(174, 64, 47, 227);
  test_sequence(248, 215, 230, 171);
  test_sequence(243, 55, 174, 35);
  test_sequence(176, 92, 16, 60);
end

always @ (posedge finish)
  $display("%d <- (%d, %d) (%d, %d)", 
    left_out,
    dut.ssn_1.reg_small, 
    dut.ssn_1.reg_large, 
    dut.ssn_2.reg_small, 
    dut.ssn_2.reg_large);
endmodule 
