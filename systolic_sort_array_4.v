// 2016 Ryan Leonard
// Basic Systolic Sorting Array (2 elements)
//
// To chain them together we need some method for not resetting the whole
// circuit but rather just resetting the 'valid' bit...
`timescale 1ns / 1ns
module systolic_sort_array_4(
  input   wire        clk,
  input   wire        reset,
  input	  wire  [7:0] in, 
  output  wire  [7:0] out,
  output  wire        finish
);

wire  [7:0] bridge_1_to_2;
wire  [7:0] bridge_2_to_1;
wire  [7:0] bridge_2_to_3;
wire  [7:0] bridge_3_to_2;
wire  [7:0] bridge_3_to_4;
wire  [7:0] bridge_4_to_3;
wire  [7:0] right_in = 8'hFF;
wire finish_1;
wire finish_2;
wire finish_3;
wire enabled_1_to_2;
wire enabled_2_to_1;
wire enabled_2_to_3;
wire enabled_3_to_2;
wire enabled_3_to_4;
wire enabled_4_to_3;

systolic_sort_node ssn_1(
  .start      (clk),
  .enabled_input_right (enabled_2_to_1),
  .reset      (reset),
  .left_in    (in), 
  .right_in   (bridge_2_to_1), 
  .left_out   (out), 
  .right_out  (bridge_1_to_2),
  .enable_out (enabled_1_to_2)
);

systolic_sort_node ssn_2(
  .start      (enabled_1_to_2),
  .enabled_input_right (enabled_3_to_2),
  .reset      (reset),
  .left_in    (bridge_1_to_2), 
  .right_in   (bridge_3_to_2), 
  .left_out   (bridge_2_to_1),
  .right_out  (bridge_2_to_3),
  .enable_out (enabled_2_to_3)
);

systolic_sort_node ssn_3(
  .start      (enabled_2_to_3),
  .enabled_input_right (enabled_3_to_2),
  .reset      (reset),
  .left_in    (bridge_2_to_3), 
  .right_in   (bridge_4_to_3), 
  .left_out   (bridge_3_to_2), 
  .right_out  (bridge_3_to_4),
  .enable_out (enabled_3_to_4)
);

systolic_sort_node ssn_4(
  .start      (enabled_3_to_4),
  .enabled_input_right (right_in_enable),
  .reset      (reset),
  .left_in    (bridge_3_to_4), 
  .right_in   (right_in), 
  .left_out   (bridge_4_to_3),
  .right_out  (),
  .enable_out (finish)
);

endmodule
