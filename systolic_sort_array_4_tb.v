// 2016 Ryan Leonard
// Basic Systolic Sorting Array (2 elements) Test Bench
`timescale 1ns / 1ns
module test_ssa4;

reg         clock;
reg         reset;
reg   [7:0] left_in;
wire  [7:0] left_out;

systolic_sort_array_4 dut(
  .clk	      (clock),
  .reset      (reset),
  .in         (left_in), 
  .out        (left_out),
  .finish     (finish)
);

initial // Drive the clock
begin
  #5 clock = 0;
  forever
    #5 clock = ~clock;
end

task test_sequence(
  input [7:0] a,
  input [7:0] b, 
  input [7:0] c,
  input [7:0] d,
  input [7:0] e,
  input [7:0] f, 
  input [7:0] g,
  input [7:0] h
);
begin
  $display("Reset Array");
  reset=1;  #10;

  $display("Inputting sequence: %d, %d, %d, %d, %d, %d, %d, %d", a, b, c, d, e, f, g, h);
  reset=0;
  left_in=a;    #10;
  left_in=b;    #10;
  left_in=c;    #10;
  left_in=d;    #10;
  left_in=e;    #10;
  left_in=f;    #10;
  left_in=g;    #10;
  left_in=h;    #10;

  $display("Flushing Array");
  left_in=255;  #80;
end
endtask

initial	// Test stimulus
begin
  reset=1;  #10;
  test_sequence(10, 9, 8, 7, 6, 5, 4, 3);
end

always @ (posedge finish)
  $display("%d <- (%d, %d) (%d, %d) (%d, %d) (%d, %d)", 
    left_out,
    dut.ssn_1.reg_small, 
    dut.ssn_1.reg_large, 
    dut.ssn_2.reg_small, 
    dut.ssn_2.reg_large,
    dut.ssn_3.reg_small, 
    dut.ssn_3.reg_large, 
    dut.ssn_4.reg_small, 
    dut.ssn_4.reg_large);
endmodule 
