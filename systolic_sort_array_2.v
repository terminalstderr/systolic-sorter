// 2016 Ryan Leonard
// Basic Systolic Sorting Array (2 elements)
//
// To chain them together we need some method for not resetting the whole
// circuit but rather just resetting the 'valid' bit...
`timescale 1ns / 1ns
module systolic_sort_array_2(
  input   wire        clk,
  input   wire        reset,
  input	  wire  [7:0] in, 
  output  wire  [7:0] out,
  output  wire        finish
);

wire  [7:0] bridge_1_to_2;
wire  [7:0] bridge_2_to_1;
wire  [7:0] right_in = 8'hFF;
wire finish_1;

systolic_sort_node ssn_1(
  .enable_in  (clk),
  .reset      (reset),
  .left_in    (in), 
  .right_in   (bridge_2_to_1), 
  .left_out   (out), 
  .right_out  (bridge_1_to_2),
  .enable_out (finish_1)
);

systolic_sort_node ssn_2(
  .enable_in  (finish_1),
  .reset      (reset),
  .left_in    (bridge_1_to_2), 
  .right_in   (right_in), 
  .left_out   (bridge_2_to_1),
  .right_out  (),
  .enable_out (finish)
);

endmodule
