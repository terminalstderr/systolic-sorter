// 2016 Ryan Leonard
// Basic Systolic Sorting Node
//
//  INSTRUCTIONS:
// The compute signal must be low for one clock cycle to ensure 
// proper flushing of the internal data structure before
// beginning execution!
//

`timescale 1ns / 1ns

module systolic_sort_node(
  input   wire        start,
  input   wire        reset,
  input   wire        enabled_input_right,
  input	  wire  [7:0] left_in, 
  input	  wire  [7:0] right_in, 
  output  reg   [7:0] left_out, 
  output  reg   [7:0] right_out,
  output  reg         enabled_output,
  output  reg         err_state,
  output  reg         finished

);

parameter 
  ON = 1'b1,
  OFF = 1'b0;

reg [7:0] reg_large;
reg [7:0] reg_small;

wire enabled_input_left = start;

  // Each clock cycle, the cell 
  // passes the smaller value to the 
  // left if it is
  // smaller than the larger value on the left
  //
  // Similarly, the larger value is passed to the right if it 
  // is bigger
  // than the smaller value on the right adjacent cell.
  //
  // if (my_smaller smaller than larger value to left)
    // pass my_smaller to the left
  // if (my_larger larger than smaller value to right)
    // pass my_larger to the right
  //

  // To implement this logic in a systolic fashion, we need lots of enable
  // bits for propogating the information. We need a bit to indicate that the
  // circuit should be starting. Then we need a bit to indicate that the
  // outputs of this node are available (sent to the right and left). finally
  // to let the outside circuit know that our circuit is actually complete,
  // we have a finish signal.
  //


  // Output updating
always @(posedge start) 
begin // BEGIN always 
  enabled_output = OFF;
  left_out  = reg_small;
  right_out = reg_large;
  enabled_output = ON;
end // END always


  // Register updating
always @(enabled_input_right)
begin
  err_state = OFF;
  if (enabled_input_left == OFF)
  begin
    err_state = ON;
  end
  else
  begin
    if (reset) 
    begin // BEGIN reset
      reg_small = 8'b0;
      reg_large = 8'b0;
      err_state = OFF;
      finished = OFF;
    end // END reset
    else
    begin
      // Case 2
      if (reg_small <   left_in &&
          left_in   <=  reg_large &&
          reg_large <=  right_in) 
      begin
        reg_small  = left_in;
      end

      // Case 3
      else if ( reg_large  <   left_in &&
                reg_large  <=  right_in)
      begin
        reg_small  = reg_large;
        reg_large  = left_in;
      end 

      // Case 4 
      else if ( left_in    <=  reg_small &&
                reg_small  <=  right_in &&
                right_in   <   reg_large)
      begin
        reg_large  = right_in;
      end 

      // Case 5
      else if ( left_in   <=  reg_small &&
                right_in  <   reg_small)
      begin
        reg_large  = reg_small;
        reg_small  = right_in;
      end 

      // Case 6
      else if ( reg_small <   left_in &&
                left_in   <=  right_in &&
                right_in  <   reg_large) 
      begin
        reg_small  = left_in;
        reg_large  = right_in;
      end 

      // Case 7
      else if ( reg_small <=  right_in &&
                right_in  <   left_in &&
                right_in  <   reg_large) 
      begin
        reg_small  = right_in;
        reg_large  = left_in;
      end
      /*
      // Case 1
      if (  left_in     <= reg_small && 
            reg_small  <= reg_large && 
            reg_large  <= right_in) begin
        // Do Nothing
      */
      finished = ON;
    end
  end
end

endmodule 
