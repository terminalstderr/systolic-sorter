// 2016 Ryan Leonard
// Basic Systolic Sorting Node Test Bench

`timescale 1ns / 1ns
module test_ssn;

localparam
  ON = 1,
  OFF = 0;

reg         start;
reg         reset;
reg         right_in_enabled;
reg   [7:0] left_in;
reg   [7:0] right_in;
wire  [7:0] left_out;
wire  [7:0] right_out;
wire        finish;
wire        enable_out;
wire        error;

systolic_sort_node dut(
  .start      (start),
  .enabled_input_right (right_in_enabled),
  .reset      (reset),
  .left_in    (left_in), 
  .left_out   (left_out), 
  .right_in   (right_in), 
  .right_out  (right_out),
  .finished   (finish),
  .enabled_output (enable_out),
  .err_state      (error)
);

task assert_equal(
  input [7:0] expected,
  input [7:0] observed);
begin
  if (expected != observed)
    $display("ASSERTION EQUAL FAIL: %p != %p", expected, observed);
end
endtask

task test_case(
  input integer id,
  input [7:0] in_left,
  input [7:0] in_right, 
  input [7:0] reg_small,
  input [7:0] reg_large,
  input [7:0] expected_small,
  input [7:0] expected_large
  );
begin
  reset = ON; 
  right_in_enabled = OFF;
  start = ON; 
  #5;
  start = OFF; 
  #5;

  $display("Case(%d): %d ->(%d,%d)<- %d --> (%d,%d)", 
    id,
    in_left, reg_small, reg_large, in_right, 
    expected_small, expected_large);
  reset=OFF;
  dut.reg_small = reg_small; 
  dut.reg_large = reg_large; 
  left_in=in_left; 
  start = ON;
  right_in = in_right;  
  right_in_enabled = ON;
  #5;
  assert_equal(dut.reg_large, expected_large);
  assert_equal(dut.reg_small, expected_small);
end
endtask

initial	// Test stimulus
begin
  // inputs, registers, outputs
  test_case(1,  8,    3,    1,    7,    3,    8);
  test_case(2,  7,    5,    3,    6,    5,    7);
  test_case(3,  6,    255,  5,    12,   6,    12);
  test_case(4,  9,    5,    3,    8,    5,    9);
  test_case(5,  9,    5,    3,    8,    5,    9);
  test_case(6,  8,    6,    5,    7,    6,    8);
  test_case(7,  6,    7,    12,   255,  7,    12);
  test_case(8,  3,    3,    3,    3,    3,    3);
  test_case(9,  1,    4,    2,    3,    2,    3);
  test_case(10, 5,    4,    2,    3,    3,    5);
  test_case(11, 5,    3,    2,    3,    3,    5);
  test_case(12, 3,    3,    3,    4,    3,    3);
  test_case(13, 2,    6,    3,    7,    3,    6);
  test_case(14, 9,    8,    9,   10,    8,    9);
  test_case(15, 2,    1,    9,   10,    1,    9);
  test_case(16, 3,    3,    2,    4,    3,    3);
  test_case(17, 3,    9,    2,   12,    3,    9);
  test_case(18, 3,    5,    0,    4,    3,    4);

end // END test stimulus

always @ (posedge finish)
  $display("(%d, %d) || %b", dut.reg_small, dut.reg_large, error);

endmodule 
