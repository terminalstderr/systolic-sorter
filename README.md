# README #

This project is written in System Verilog and has been simulated in the ModelSim environment.

### What is this repository for? ###

This project is an investigation of the systolic sorting IP specified by [Song et al.](https://ll.mit.edu/publications/journal/pdf/vol20_no1/20_1_7_Song.pdf)
Initial investigations are to look into several 'k' values for the k-way merge sorter.
Our goal is to see if there is an optimal 'k' value based on system specifications.

### Who do I talk to? ###

Contact Ryan Leonard for information.

ryan.leonard71@gmail.com