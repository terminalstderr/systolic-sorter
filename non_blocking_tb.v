// 2016 -- Ryan Leonard 
`timescale 1ns / 1ns
module test_non_blocking;
reg a_in, clk;
wire c_out;
non_blocking dut(
  .A_in	  (a_in),
  .clk    (clk),
  .C_out	(c_out)
);

// Test stimulus
initial	
  begin
    a_in = 0;
    #50 a_in = 1;
    #200 a_in = 0;
  end

// Clock generator
initial 
  begin
    clk = 0;
    forever #10 clk = !clk;
  end

// Print output to screen!
initial 
  begin
    //$display("time, ain, cout"); 
    //$monitor("%d, %b, %b",$time, a_in, c_out); 
  end

endmodule 

