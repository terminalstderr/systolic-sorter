// 2016 -- Ryan Leonard 
module non_blocking(
  input   A_in, 
  input   clk,
  output	reg C_out
  
);
  wire B_in, C_in;
  reg A_out, B_out;

  always @ (posedge clk)
  begin
    A_out = A_in;
  end

  assign B_in = A_out + 1;

  always @ (posedge clk)
  begin
    B_out = B_in;
  end

  assign C_in = B_out + 1;

  always @ (posedge clk)
  begin
    C_out = C_in;
  end

initial 
  begin
    $display("a_out, b_out, c_out"); 
    $monitor("%b, %b, %b",C_out, B_out, C_out); 
  end

endmodule
